Overview
========

Brutal Maze is a thrilling shoot 'em up game with minimalist art style.

.. image:: images/screenshot.png

Notable features:

* Being highly portable.
* Auto-generated and infinite maze.
* No binary data for drawing.
* Enemies with special abilities: stun, poison, camo, etc.
* Somewhat a realistic physic and logic system.
* Resizable game window in-game.
* Easily customizable via INI file format.
* Recordable in JSON (some kind of silent screencast).
* Remote control through TCP/IP socket (can be used in AI researching).

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2

   install
   config
   gameplay
   remote
   copying

Record Player
-------------

.. raw:: html

   <iframe src='recplayer.html' width=640 height=480></iframe>
