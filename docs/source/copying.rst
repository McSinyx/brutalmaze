Copying
=======

This listing is our best-faith, hard-work effort at accurate attribution,
sources, and licenses for everything in Brutal Maze.  If you discover
an asset/contribution that is incorrectly attributed or licensed,
please contact us immediately.  We are happy to do everything we can
to fix or remove the issue.

License
-------

Brutal Maze's source code and its icon are released under GNU Affero General
Public License version 3 or later.  This means if you run a modified program on
a server and let other users communicate with it there, your server must also
allow them to download the source code corresponding to the modified version
running there.

.. image:: https://www.gnu.org/graphics/agplv3-155x51.png
   :target: https://www.gnu.org/licenses/agpl.html

Other creative works retain their original licenses as listed below.

Color Palette
-------------

Brutal Maze uses the Tango color palette by `the Tango desktop project`_
to draw all of its graphics.  The palette is released to the Public Domain.

Sound Effects
-------------

Sound Effects Artist---Tobiasz 'unfa_' Karoń

* License: `CC BY 3.0`_
* brutalmaze/soundfx/heart.ogg (original__)

__ https://freesound.org/s/217456

Sound Effects Artist---HappyParakeet_

* License: `CC0 1.0`_
* brutalmaze/soundfx/lose.ogg (original__)

__ https://freesound.org/s/398068

Sound Effects Artist---jameswrowles_

* License: `CC0 1.0`_
* brutalmaze/soundfx/missed.ogg (original__)

__ https://freesound.org/s/380641

Sound Effects Artist---MrPork_

* License: `CC0 1.0`_
* brutalmaze/soundfx/noise.ogg (original__)

__ https://freesound.org/s/257449

Sound Effects Artist---suspensiondigital_

* License: `CC0 1.0`_
* brutalmaze/soundfx/shot-enemy.ogg (original__)

__ https://freesound.org/s/389704

Sound Effects Artist---gusgus26_

* License: `CC0 1.0`_
* brutalmaze/soundfx/shot-hero.ogg (original__)

__ https://freesound.org/s/121188

Sound Effects Artist---braqoon_

* License: `CC0 1.0`_
* brutalmaze/soundfx/slash-enemy.ogg (original__)

__ https://freesound.org/s/161098

Sound Effects Artist---Qat_

* License: `CC0 1.0`_
* brutalmaze/soundfx/slash-hero.ogg (original__)

__ https://freesound.org/s/108333

Sound Effects Artist---pepingrillin_

* License: `CC0 1.0`_
* brutalmaze/soundfx/spawn.ogg (original__)

__ https://freesound.org/s/252083

.. _CC BY 3.0: https://creativecommons.org/licenses/by/3.0/legalcode
.. _CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/legalcode
.. _CC BY-SA 3.0: https://creativecommons.org/licenses/by-sa/3.0/legalcode

.. _the Tango desktop project: http://tango-project.org/
.. _unfa: https://freesound.org/people/unfa/
.. _HappyParakeet: https://freesound.org/people/HappyParakeet/
.. _jameswrowles: https://freesound.org/people/jameswrowles/
.. _MrPork: https://freesound.org/people/MrPork/
.. _suspensiondigital: https://freesound.org/people/suspensiondigital/
.. _gusgus26: https://freesound.org/people/gusgus26/
.. _braqoon: https://freesound.org/people/braqoon/
.. _Qat: https://freesound.org/people/Qat/
.. _pepingrillin: https://freesound.org/people/pepingrillin/
